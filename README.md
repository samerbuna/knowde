# KNOWDE

## Running

To run the development server:

```sh
npm install

npm run dev:bundler # this will re-bundle on save

npm run dev:server # runs on port 1234 by default
```

To run all the tests:

```sh
npm test
```

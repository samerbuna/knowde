export function getproductId() {
  // RANDOM PRODUCT: 1-5 are valids, 6-10 are not valid
  return 'PRODUCT-' + Math.floor(1 + 10 * Math.random());
}

export function priceDisplay(priceInCents, digits = 2) {
  return `$${(priceInCents / 100).toFixed(digits)}`;
}

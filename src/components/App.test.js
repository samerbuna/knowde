import * as React from 'react';
import fetch from 'cross-fetch';
import '@testing-library/jest-dom/extend-expect';

import App from './App';

import { render, fireEvent, screen, act } from '@testing-library/react';

jest.mock('cross-fetch');

describe('App', () => {
  it('renders a scan button that gets a product', async () => {
    const { asFragment } = render(<App />);

    const button1 = screen.getByTitle('scan');
    fetch.mockResolvedValue({
      status: 200,
      json: () => ({ productData: { name: 'TEST', priceInCents: 1000 } }),
    });
    await act(async () => {
      fireEvent.click(button1);
    });

    expect(asFragment()).toMatchSnapshot();
  });
  it('renders a scan button that renders an error', async () => {
    const { asFragment } = render(<App />);

    const button1 = screen.getByTitle('scan');
    fetch.mockResolvedValue({
      status: 200,
      json: () => ({ err: 'ERROR' }),
    });
    await act(async () => {
      fireEvent.click(button1);
    });

    expect(asFragment()).toMatchSnapshot();
  });
  it('renders an exit button', async () => {
    const { asFragment } = render(<App />);

    const button2 = screen.getByTitle('exit');
    await act(async () => {
      fireEvent.click(button2);
    });
    expect(asFragment()).toMatchSnapshot();
  });
});

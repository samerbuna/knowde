import * as React from 'react';
import fetch from 'cross-fetch';

import LCDDisplay from './LCDDisplay';
import PrinterDisplay from './PrinterDisplay';
import { getproductId } from 'utils';

function useStore() {
  const [error, setError] = React.useState(null);
  const [currentProduct, setCurrentProduct] = React.useState(null);
  const [scannedProducts, setScannedProducts] = React.useState({
    list: [],
    total: 0,
  });
  const [done, setDone] = React.useState(false);
  const scanProduct = async () => {
    setDone(false); // For scanning after exit
    const resp = await fetch('/api/scanProduct', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ productId: getproductId() }),
    });
    const { err, productData } = await resp.json();
    if (err) {
      setError(err);
      return;
    }
    setError(null);
    setCurrentProduct(productData);
    setScannedProducts((products) => {
      const productAlreadyScanned = scannedProducts.list.find(
        (product) => product.id === productData.id,
      );
      // Don't add the same product twice
      if (productAlreadyScanned) {
        return products;
      }
      // Cache the total so that it does not get calculated when done is true
      products.total = products.total + productData.priceInCents;
      products.list = products.list.concat(productData);
      return products;
    });
  };
  return {
    scanProduct,
    exitScan: () => setDone(true),
    currentProduct,
    scannedProducts,
    error,
    done,
  };
}

export default function App() {
  const {
    scanProduct,
    exitScan,
    currentProduct,
    scannedProducts,
    error,
    done,
  } = useStore();
  return (
    <div className="app">
      <div className="buttons">
        <button title="scan" onClick={scanProduct}>
          SCAN
        </button>
        <button title="exit" onClick={exitScan}>
          EXIT
        </button>
      </div>
      <hr />
      <div className="grid">
        <div>
          <h2>LCD</h2>
          <LCDDisplay
            currentProduct={currentProduct}
            scannedProducts={scannedProducts}
            error={error}
            done={done}
          />
        </div>
        <div>
          <h2>PRINTER</h2>
          {done && <PrinterDisplay scannedProducts={scannedProducts} done />}
        </div>
      </div>
    </div>
  );
}

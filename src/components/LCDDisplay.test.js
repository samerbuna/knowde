import * as React from 'react';
import '@testing-library/jest-dom/extend-expect';

import LCDDisplay from './LCDDisplay';

import { render } from '@testing-library/react';

describe('LCDDisplay', () => {
  it('renders nothing', () => {
    const { asFragment } = render(<LCDDisplay />);
    expect(asFragment()).toMatchSnapshot();
  });
  it('renders an error', () => {
    const { getByText, asFragment } = render(<LCDDisplay error="ERROR" />);
    expect(getByText('ERROR')).toMatchInlineSnapshot(`
      <div
        class="error"
      >
        ERROR
      </div>
    `);

    expect(asFragment()).toMatchSnapshot();
  });
  it('renders total', () => {
    const { getByText, asFragment } = render(
      <LCDDisplay
        scannedProducts={{
          list: [
            { id: 'test1', name: 'TEST1', priceInCents: 2021 },
            { id: 'test2', name: 'TEST2', priceInCents: 2022 },
          ],
          total: 0,
        }}
        done={true}
      />,
    );
    expect(getByText(/Total: /)).toMatchInlineSnapshot(`
      <div>
        Total: 
        $0.00
      </div>
    `);

    expect(asFragment()).toMatchSnapshot();
  });
  it('renders current product', () => {
    const { getByText, asFragment } = render(
      <LCDDisplay
        currentProduct={{
          id: 'test',
          name: 'TEST',
          priceInCents: 2021,
        }}
      />,
    );
    expect(getByText(/Product Name/)).toMatchInlineSnapshot(`
      <div>
        Product Name: 
        TEST
      </div>
    `);

    expect(asFragment()).toMatchSnapshot();
  });
});

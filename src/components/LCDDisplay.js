import * as React from 'react';

import { priceDisplay } from 'utils';

export default function LCDDisplay({
  currentProduct,
  scannedProducts,
  error,
  done,
}) {
  if (error) {
    return <div className="error">{error}</div>;
  }
  if (done) {
    return <div>Total: {priceDisplay(scannedProducts.total)}</div>;
  }
  if (currentProduct) {
    return (
      <div>
        <div>Product Name: {currentProduct.name}</div>
        <div>Product Price: {priceDisplay(currentProduct.priceInCents)}</div>
      </div>
    );
  }
  return null;
}

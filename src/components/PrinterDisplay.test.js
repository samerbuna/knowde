import * as React from 'react';
import '@testing-library/jest-dom/extend-expect';

import PrinterDisplay from './PrinterDisplay';

import { render } from '@testing-library/react';

describe('PrinterDisplay', () => {
  it('renders for an empty list', () => {
    const { asFragment } = render(
      <PrinterDisplay scannedProducts={{ list: [] }} />,
    );
    expect(asFragment()).toMatchSnapshot();
  });
  it('renders for a valid list', () => {
    const { asFragment } = render(
      <PrinterDisplay
        scannedProducts={{
          list: [{ id: 'test', name: 'TEST', priceInCents: 2021 }],
          total: 0,
        }}
      />,
    );
    expect(asFragment()).toMatchSnapshot();
  });
});

import * as React from 'react';

import { priceDisplay } from 'utils';

export default function PrinterDisplay({ scannedProducts }) {
  return (
    <div>
      {scannedProducts.list.map((product) => (
        <div className="product" key={product.id}>
          <div>Product Name: {product.name}</div>
          <div>Product Price: {priceDisplay(product.priceInCents)}</div>
        </div>
      ))}
      <br />
      <div>Total: {priceDisplay(scannedProducts.total)}</div>
    </div>
  );
}

// Database mock object
const PRODUCTS = {
  'PRODUCT-1': {
    name: 'PRODUCT ONE',
    priceInCents: 1050,
  },
  'PRODUCT-2': {
    name: 'PRODUCT TWO',
    priceInCents: 2042,
  },
  'PRODUCT-3': {
    name: 'PRODUCT THREE',
    priceInCents: 3775,
  },
  'PRODUCT-4': {
    name: 'PRODUCT FOUR',
    priceInCents: 4200,
  },
  'PRODUCT-5': {
    name: 'PRODUCT FIVE',
    priceInCents: 5070,
  },
};

export function getProductData(productId) {
  const productData = PRODUCTS[productId];

  if (!productData) {
    throw Error('Product not found');
  }

  productData.id = productId;

  return productData;
}

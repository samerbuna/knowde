const env = process.env;

export default {
  port: env.PORT || 1234,
  host: env.HOST || 'localhost',
};

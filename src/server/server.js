import config from 'server/config';
import { app } from 'server/express';
import apiRoutes from 'server/routes/api';

app.get('/', async (req, res) => {
  res.render('index');
});

app.use('/api', apiRoutes);

app.listen(config.port, config.host, () => {
  console.info(`Running on ${config.host}:${config.port}...`);
});

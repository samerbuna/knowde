import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';

export const app = express();

app.enable('trust proxy');
app.use(morgan('common'));

app.use(express.static('public'));

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

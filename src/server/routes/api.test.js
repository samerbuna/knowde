import request from 'supertest';

import { app } from 'server/express';
import apiRoutes from 'server/routes/api';

app.use('/api', apiRoutes);

describe('/scanProducts', () => {
  it('errs for missing product id', (done) => {
    request(app)
      .post('/api/scanProduct')
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.err).toBe('Invaild request');
        done();
      })
      .catch((err) => done(err));
  });
  it('errs for invalid product id', (done) => {
    request(app)
      .post('/api/scanProduct')
      .send({ productId: '404' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.err).toBe('Product not found');
        done();
      })
      .catch((err) => done(err));
  });
  it('returns name and price for a valid product id', (done) => {
    request(app)
      .post('/api/scanProduct')
      .send({ productId: 'PRODUCT-1' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body.err).toBeUndefined;
        const { productData } = response.body;
        expect(productData.name).toBe('PRODUCT ONE');
        expect(productData.priceInCents).toBe(1050);
        done();
      })
      .catch((err) => done(err));
  });
});

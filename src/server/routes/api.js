import express from 'express';

import { getProductData } from 'server/db';

const router = express.Router({ caseSensitive: true });

router.post('/scanProduct', (req, res) => {
  try {
    const { productId } = req.body;
    if (!productId) {
      throw Error('Invaild request');
    }
    const productData = getProductData(productId);
    res.send({ productData });
  } catch (err) {
    res.send({ err: err.message });
  }
});

export default router;
